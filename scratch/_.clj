(def rooms {:default [1 2]})
(def clients {1 :a 2 :b})

; (swap! rooms update :default (filter #(not= 1 %)))

(update rooms :default (partial into [] (comp
                                          (map clients)
                                          (filter some?)
                                          (filter #(= :a %)))))