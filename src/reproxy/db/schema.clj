(ns reproxy.db.schema
  (:require [datahike.api :as d]
            [reproxy.db.connection :refer [conn]]))

(def schema [{:db/ident       :user/id
              :db/valueType   :db.type/string
              :db/unique      :db.unique/identity
              :db/index       true
              :db/cardinality :db.cardinality/one}
             {:db/ident       :user/password
              :db/valueType   :db.type/string
              :db/cardinality :db.cardinality/one}
             {:db/ident       :user/deleted?
              :db/valueType   :db.type/boolean
              :db/cardinality :db.cardinality/one}])

(d/transact! conn schema)
(println "Schema created")