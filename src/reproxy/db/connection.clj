(ns reproxy.db.connection
  (:require [datahike.api :as d]
            [reproxy.config :refer [config]]))

(def conn (d/connect (config :database)))