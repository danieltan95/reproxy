(ns reproxy.db.init
  (:require [datahike.api :as d]
            [reproxy.config :refer [config]]))

;; expected to run once
(let [db-config (config :database)]
  ; (d/delete-database db-config)
  (d/create-database db-config)
  (print "Database created"))