(ns reproxy.config
  (:require [clojure.tools.reader.edn :as edn]
            [clojure.java.io :as io]))

(defn read-edn-config [file-name]
  (->> file-name
       (io/resource)
       (slurp)
       (edn/read-string)))

(def config (read-edn-config "config.edn"))
