(ns reproxy.core
  (:require
    [reitit.ring :as ring]
    [org.httpkit.server :as h]
    [reproxy.handlers.api :as api])
  (:gen-class))

(defonce server (atom nil))

(defn stop-server []
  (println "stopping ...")
  (when-not (nil? @server)
    ;; graceful shutdown: wait 100ms for existing requests to be finished
    ;; :timeout is optional, when no timeout, stop immediately
    (@server :timeout 100)
    (reset! server nil)))

(defn -main
  []
  (stop-server)
  (reset! server
          (h/run-server
            (ring/ring-handler
              (ring/router
                [["/api"
                  ["/begin-channel" {:get {:handler api/begin-channel}}]]])
              (ring/routes
                (ring/create-default-handler)))
            {:port 8080}))
  (println "running..."))
