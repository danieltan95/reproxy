(ns reproxy.handlers.api
  (:require [org.httpkit.server :as h]
            [reproxy.config :refer [config]]))

(def clients (atom {}))

(def rooms (atom {:default []}))

(add-watch rooms :default
  (fn [_key _atom old-state new-state]
    (println "old:" old-state)
    (println "new:" new-state)))

(defn uuid [] (.toString (java.util.UUID/randomUUID)))

(defn begin-channel [req]
  (if-not (:websocket? req)
    {:status 200 :body "Welcome! JS client connecting..."}
    (h/as-channel req
      {:on-open (fn [ch]
                  (let [id (uuid)]
                    (swap! rooms update :default conj id)
                    (swap! clients assoc id ch)
                    (println
                      "opened with channel :" id)))
       :on-receive (fn [ch message]
                    (println "received: " message)
                    (doseq [id (@rooms :default)]
                      (let [user-ch (@clients id)]
                        (if (= ch user-ch)
                         (h/send! ch "sent")
                         (h/send! user-ch message)))))
       :on-close (fn [ch status]
                  (println "closed: " status)
                  (swap! rooms update :default (partial into [] (comp
                                                                  (map @clients)
                                                                  (filter some?)
                                                                  (filter #(not= ch %)))))
                  (doseq [id (@rooms :default)]
                    (h/send! (@clients id) (pr-str {:left id
                                                    :status status}))))})))

